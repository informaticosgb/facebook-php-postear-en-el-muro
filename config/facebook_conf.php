<?php 

// Definimos un arreglo con la configuracion de nuestra app
$config = array(

	'app_id' => 'TU_APP_ID',
	'app_secret' => 'TU_APP_SECRET_ID',
	
	// definimos los scopes a usar en nuestra app, tambien en otro arreglo
	// requiere el scope publish_actions para poder publicar en el muro
	'scopes' => array('email','read_friendlists','user_online_presence', 'publish_actions')

	);

 ?>
