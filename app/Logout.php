<?php 
	
	// reanudamos session
	session_start();

	// eliminamos y destruimos sessions.
	session_unset();
	session_destroy();

	// redireccionamos a index.php
	header("location: ../index.php");

 ?>
