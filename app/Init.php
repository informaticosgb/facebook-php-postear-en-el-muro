<?php 

	/*
	* Octubre 2014.
	* 
	* Ejemplo feo de Facundo Albarracin :p
	*
	* Testeado en localhost con XAMPP.
	*
	* Testeado en Linux y Windows. 
	*
	* Links de relevancia: https://developers.facebook.com/, 
	* https://developers.facebook.com/docs/php/gettingstarted/4.0.
	* 	
	* Tengan en cuenta como estructuran los directorios del proyecto.		
	*	
	*/
	
	// Iniciamos sessión
	session_start();

	// Nuestra app requiere estos archivos para que funcione.
	require_once 'config/facebook_conf.php';
	require_once 'vendor/autoload.php';

	// las clases del vendor a usar.
	use Facebook\FacebookSession;
	use Facebook\FacebookRedirectLoginHelper;
	use Facebook\FacebookRequest;
	use Facebook\FacebookRequestException;
	use Facebook\GraphObject;
	use Facebook\GraphLocation;
	use Facebook\GraphUser;

	// establecemos la configuracion de nuesta app con el app_id y el app_secret
	FacebookSession::setDefaultApplication($config['app_id'], $config['app_secret']);

	// creamos una instancia de FacebookRedirectLoginHelper con la url de nuestro servidor local 
	// pasandolo como parametro al constructor. 
	$helper = new FacebookRedirectLoginHelper('http://localhost/facebook/index.php');

	try {

		// obtenemos la session del redireccionamiento
		// getSessionFromRedirect devuelve una session o null
		$session = $helper->getSessionFromRedirect();

		// si hay redireccionamiento hacemos esto

		if ($session) {
			
			// creamos una session y obtenemos su toquen
			$_SESSION['facebook'] = $session->getToken();
			$mensa = 'Suscribite a mi canal ;).';
			$link = 'https://www.youtube.com/user/InformaticosGB';

			// y posteamos en el muro	
			$response = (new FacebookRequest(
	      	$session, 'POST', '/me/feed', array(
	        		
	        		'link' => $link,
		          	'message' => $mensa
		    	)
	    	))->execute()->getGraphObject(); // ejecutamos la orden

	    	//echo "Posteado con id: ".$response->getProperty('id');
    		echo 'Revisá tu muro ;)';

			// redireccionamos con un refresh de 3 segundos
		  	header('refresh: 3; url = index.php');

		}

		// si la sessión está establecida y no es nula
		if (isset($_SESSION['facebook'])) {
			
			// Hacemos peticion al Graph API
			$session = new FacebookSession($_SESSION['facebook']);
		  	$me = (new FacebookRequest(
  			$session, 'GET', '/me'
			))->execute()->getGraphObject(GraphUser::className());

			$facebook_user = $me->getName(); // y obtenemos el nombre del usuario

		}

		// Lanzamos exceptions si nuestra app falla en algun momento
	} catch(FacebookRequestException $ex) {

		$ex->getMessage();

	} catch(\Exception $ex) {
		  
		$ex->getMessage();

	}

 ?>
