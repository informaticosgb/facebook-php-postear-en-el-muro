<?php 
	
	// nuestra app requiere el archivo Init.php
	require_once 'app/Init.php';

 ?>

<!DOCTYPE html>
<html lang="en">
<head>

	<!-- Usamos bootstrap -->

	<meta charset="UTF-8">
	<title>FacebookTest</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<style>
			
		/*Algo de estilo, sino queda muy feo*/	
		body{
			
			margin: 100px auto;
			text-align: center;

		}
		
	</style>
</head>
<body>

	<h2>PHP Facebook Test</h2>
	
	<?php // si aún no hay session mostramos el boton de iniciar
	if (!isset($_SESSION['facebook'])): ?>
			
		<a href="<?php echo $helper->getLoginUrl($config['scopes']); ?>" class="btn btn-primary">Iniciar sesión y postear :D</a>
		
	<?php // si ya hay session mostramos bienvenido seguido del nombre del usuario
	else: ?>
		
		<p>
			
			Bienvenido, <?php echo $facebook_user; ?>

		</p>

		<a href="app/Logout.php" class="btn btn-danger">Salir</a>	
		<!--<a href="app/Post.php" class="btn btn-success">Hacer un post</a>-->

	<?php // finalizamos la condicion
	endif; ?>
	

</body>
</html>
